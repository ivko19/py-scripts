﻿# Given n non-negative integers a1, a2, ..., an , where each represents a point at coordinate (i, ai).
# n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
# Find two lines, which together with x-axis forms a container, such that the container contains the most water.
# Note: You may not slant the container and n is at least 2.
# https://leetcode.com/problems/container-with-most-water/

# input list
perm_list=[]
perm_dict={}
i=1
while True:
    print ("gimme nums:")
    char = input()
    if char =="":
        break
    char = int(char)
    d={i:char}
    perm_dict.update(d)
    # print(f"this is {i}th pass")
    i+=1
    perm_list.append(char)

# print(perm_list)
# print(perm_dict)

temp_dict=perm_dict.copy()

sorted_keys = []
while temp_dict:
    # key with max value
    max_value = max(temp_dict.values())
    kljuc = [k for k, v in temp_dict.items() if v == max_value]
    # print(kljuc)

    # pop keys/items with highest value and put keys in a list
    for key in kljuc:
        temp_dict.pop(key)
        sorted_keys.append(key)

# print(temp_dict)
# print(sorted_keys)
print(perm_dict)

pocetak = 1
kraj = len(perm_list)
area=0
i=1
for sortedk in sorted_keys:
    ## cela ova ideja je da smanjis broj provera, ali ima bag - area može da preraste realnu velicinu i pada u vodu
    # rastojanje = max ((sortedk-pocetak),(kraj-sortedk))
    # if (sortedk-pocetak)>(kraj-sortedk):
    #     visika=perm_dict[pocetak]
    #     else:
    #     visika = perm_dict[kraj]
    # altituda= min (perm_dict[sortedk],visika)
    # if perm_dict(sortedk) * rastojanje > area:
    #     area = perm_dict(sortedk) * rastojanje   (ovo može da bude nerealno)
    for rest in sorted_keys[i:-1]:
        visina = min(perm_dict[sortedk],perm_dict[rest])
        duzina = abs(sortedk-rest)
        if duzina*visina > area:
            area = duzina*visina
i+=1

print(f"area je: {area}")

