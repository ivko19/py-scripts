import os
import datetime
import csv

from pydub import AudioSegment

outputPath = "./output/"

# expecting 16 bit PCM 44100 Hz
# this will be deleted after refactor
wavFile = 'Riblja Corba - Kost U Grlu.wav'


def cropAudio(oldFileName, newFileName, number, time1, time2):
    print(f"stari:{oldFileName}, Novi:{newFileName}")
    time1 = time1 / 1000  # Works in milliseconds
    time2 = time2 / 1000
    newAudio = AudioSegment.from_wav(oldFileName)
    newAudio = newAudio[time1:time2]
    newAudio.export(f"{outputPath}{number} - {newFileName}.wav", format="wav")  # Exports to a wav file in the current path.


# TODO make path dynamic
with open('splitting_instructions.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0

    try:
        os.mkdir(outputPath)
    except OSError:
        print("Creation of the directory %s failed" % outputPath)
    else:
        print("Successfully created the directory %s " % outputPath)

    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        else:
            p = datetime.datetime.strptime(row[2], '%M:%S:%f')
            t = datetime.datetime.strptime(row[3], '%M:%S:%f')
            p = p.microsecond + p.second * 1000000 + p.minute * 60 * 1000000
            t = t.microsecond + t.second * 1000000 + t.minute * 60 * 1000000

            print(f'broj pesme: {row[0]}, naziv: {row[1]}, početak {p},{type(p)} kraj {t},{type(t)}')

            cropAudio(wavFile, row[1], row[0], p, t)
            line_count += 1

    print(f'Processed {line_count} lines.')
